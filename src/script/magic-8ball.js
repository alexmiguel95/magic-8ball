const getPhrases = () => {
    const listPhrases = ["Certo", "Verdade", "Sem dúvida", "Sim", "Tente novamente", "Não"];
    return listPhrases;
}

const getValueRandom = () => {
    let random = Math.floor(Math.random() * getPhrases().length);
    return random;
}

function getPhrasesRandom(){
    const phrase = getPhrases();
    const position = getValueRandom();
    return phrase[position];
}


function defaultBall(){
    const elementParent = document.getElementById("container");
    
    // Remover elemento anterior
    document.getElementById("magic").remove();

    const createNewElement = document.createElement("div");
    createNewElement.id = "magic";
    createNewElement.style.width = "145px";
    createNewElement.style.height = "145px";
    createNewElement.style.backgroundColor = "white";
    createNewElement.style.borderRadius = "50%";
    createNewElement.style.display = "flex";
    createNewElement.style.justifyContent = "center";
    createNewElement.style.alignItems = "center";
    elementParent.appendChild(createNewElement);

    const newElement = document.createElement("span");
    newElement.style.color = "black";
    newElement.style.textAlign = "center";
    newElement.style.fontSize = "50px";
    newElement.innerText = "8";

    createNewElement.appendChild(newElement);
}

defaultBall();

function toSpin(){
    let phrase = getPhrasesRandom();

    const elementParent = document.getElementById("container");
    
    // Remover elemento anterior
    document.getElementById("magic").remove();

    // Recriando o elemento
    const createNewElement = document.createElement("div");
    createNewElement.id = "magic";
    createNewElement.style.width = "0";
    createNewElement.style.height = "0";
    createNewElement.style.borderLeft = "70px solid transparent"
    createNewElement.style.borderRight = "70px solid transparent";
    createNewElement.style.borderTop = "90px solid #3056C4";
    createNewElement.style.display = "flex";
    createNewElement.style.justifyContent = "center";
    createNewElement.style.alignItems = "center";
    elementParent.appendChild(createNewElement);

    const newElement = document.createElement("span");
    newElement.style.color = "white";
    newElement.style.marginTop = "-115px";
    newElement.style.fontSize = "15px";
    newElement.style.textAlign = "center";
    newElement.innerText = phrase;

    createNewElement.appendChild(newElement);
}

/* Ouvinte de eventos */
const listener = document.getElementById("container");
listener.addEventListener('click', toSpin);
listener.addEventListener('mouseout', defaultBall);